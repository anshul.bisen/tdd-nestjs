export default async () => {
  console.log('Initiating jest tear down...');
  if (global.__TEST_MONGO__) {
    console.log('Shutting mongo db.');
    const result = await global.__TEST_MONGO__.stop();
    console.log('Mongo db has been shut:', result);
  }
  console.log('Jest tear down done.');
};
