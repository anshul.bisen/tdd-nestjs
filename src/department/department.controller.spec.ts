import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentController } from './department.controller';
import { CreateDepartmentDto } from './dto/create-department.dto';
import { DepartmentService } from './department.service';
import { Department } from './model/department';
jest.mock('./department.service');

describe('DepartmentController', () => {
  let controller: DepartmentController;
  let service: DepartmentService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DepartmentController],
      providers: [DepartmentService],
    }).compile();

    controller = module.get<DepartmentController>(DepartmentController);
    service = module.get<DepartmentService>(DepartmentService);
  });

  it('should create department', async () => {
    //Arrange
    const createDepartmentDto = new CreateDepartmentDto();
    createDepartmentDto.name = 'SHIELD';
    service.save = jest.fn().mockResolvedValue(createDepartmentDto);

    //Act
    const savedDepartment: CreateDepartmentDto = await controller.save(
      createDepartmentDto,
    );

    //Assert
    expect(service.save).toBeCalledWith(createDepartmentDto);
    expect(savedDepartment).toEqual(createDepartmentDto);
  });

  it('should get an existing department by id', async () => {
    //Arrange
    const departmentId = 'd1';
    service.findById = jest
      .fn()
      .mockResolvedValue({ id: departmentId, name: 'SHIELD' });

    //Act
    const department: Department = await controller.findById(departmentId);

    //Assert
    expect(service.findById).toBeCalledWith(departmentId);
  });
});
