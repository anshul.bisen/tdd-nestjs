import supertest from 'supertest';
import { INestApplication, HttpStatus } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
/** nock: library to mock network calls */
import nock from 'nock';

import { providers, COLLECTION_MAPPING } from './dependency_provider';
import { DepartmentController } from '../department.controller';

describe('Departments', () => {
  let app: INestApplication;
  let server;
  /** get testDbUrl, you may also pass db url of dev/staging here */
  const testDbUrl = `${process.env.TEST_DB_URL}/${process.env.TEST_DB_NAME}?directConnection=true`;

  beforeAll(async () => {
    /** disable network calls */
    nock.disableNetConnect();
    /** enable network call for localhost */
    nock.enableNetConnect('127.0.0.1');
    await mongoose.connect(testDbUrl);
  });

  beforeEach(async () => {
    const schemaDefinitions = Object.values(COLLECTION_MAPPING).map(
      (schemaDef) => ({ name: schemaDef.name, schema: schemaDef.schema }),
    );
    /** create testing environment context*/
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(testDbUrl, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
        }),
        MongooseModule.forFeature(schemaDefinitions),
      ],
      providers,
      controllers: [DepartmentController],
    }).compile();
    app = module.createNestApplication();
    await app.init();
    /** get http server to call apis */
    server = app.getHttpServer();
  });

  afterEach(async () => {
    server.close();
  });

  it(`/GET departments by name`, async () => {
    //Arrange
    const scope = nock('http://some-test-end-point.com')
      .persist()
      .get('/department/Avengers/hod')
      .reply(() => [200, 'Captain America']);

    //Act and Assert
    const request = await supertest(server).get('/departments/Avengers/hod');
    expect(request).toEqual(
      expect.objectContaining({
        body: { hod: 'Captain America' },
        status: HttpStatus.OK,
      }),
    );
  });

  afterAll(async () => {
    await app.close();
  });
});
