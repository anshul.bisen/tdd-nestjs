import { Department } from './department';
import { Employee } from '../../employee/model/employee';

export class DepartmentWithEmployees extends Department {
  employees: Employee[];
}
