import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type DepartmentDocument = Department & Document;

@Schema()
export class Department {
  id: string;
  @Prop()
  name: string;

  @Prop()
  createdAt: Date;
}

export const DepartmentSchema = SchemaFactory.createForClass(Department);
