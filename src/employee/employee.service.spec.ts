import { Test, TestingModule } from '@nestjs/testing';

import { EmployeeService } from './employee.service';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { EmployeeRepository } from './employee.repository';
import { Employee } from './model/employee';

jest.mock('./employee.repository');

describe('EmployeeService', () => {
  let service: EmployeeService;
  let repository: EmployeeRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [EmployeeService, EmployeeRepository],
    }).compile();

    service = module.get<EmployeeService>(EmployeeService);
    repository = module.get<EmployeeRepository>(EmployeeRepository);
  });

  it('should save the Department by calling Repository', async () => {
    //Arrange
    const createEmployeeDto = new CreateEmployeeDto();
    createEmployeeDto.name = 'SHIELD';
    repository.save = jest.fn().mockResolvedValue(createEmployeeDto);

    //Act
    const result: Employee = await service.save(createEmployeeDto);

    //Assert
    expect(repository.save).toBeCalledWith(createEmployeeDto);
    expect(result).toBe(createEmployeeDto);
  });

  it('should get all employees by departmentId', async () => {
    //Arrange
    const departmentId = 'D1';

    //Act
    const employees: Employee[] = await service.find(departmentId);

    //Assert
    expect(repository.find).toBeCalledWith(departmentId);
  });
});
