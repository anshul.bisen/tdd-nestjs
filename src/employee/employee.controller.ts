import { Controller, Body, Post, Get, Param } from '@nestjs/common';

import { CreateEmployeeDto } from './dto/create-employee.dto';
import { EmployeeService } from './employee.service';
import { CreateEmployeeDtoPipe } from './pipes/create-employee-dto.pipe';
import { Employee } from './model/employee';

@Controller('employee')
export class EmployeeController {
  constructor(private readonly employeeService: EmployeeService) {}
  @Post()
  save(
    @Body(CreateEmployeeDtoPipe) createEmployeeDto: CreateEmployeeDto,
  ): Promise<Employee> {
    return this.employeeService.save(createEmployeeDto);
  }

  @Get()
  async find(departmentId: string): Promise<Employee[]> {
    return this.employeeService.find(departmentId);
  }

  @Get('/departments/:name')
  async findByName(@Param('name') name): Promise<Employee[]> {
    return await this.employeeService.find(name);
  }

  @Get('/:name/realName')
  async getRealName(@Param('name') name): Promise<object> {
    return { name: await this.employeeService.findByName(name) };
  }
}
