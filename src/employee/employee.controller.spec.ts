import { Test, TestingModule } from '@nestjs/testing';

import { EmployeeController } from './employee.controller';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { EmployeeService } from './employee.service';
import { Employee } from './model/employee';

jest.mock('./employee.service');

describe('EmployeeController', () => {
  let controller: EmployeeController;
  let service: EmployeeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EmployeeController],
      providers: [EmployeeService],
    }).compile();

    controller = module.get<EmployeeController>(EmployeeController);
    service = module.get<EmployeeService>(EmployeeService);
  });

  it('should create department', async () => {
    //Arrange
    const createEmployeeDto = new CreateEmployeeDto();
    createEmployeeDto.name = 'SHIELD';
    createEmployeeDto.post = 'manager';
    createEmployeeDto.departmentId = 'D1';
    service.save = jest.fn().mockResolvedValue(createEmployeeDto);

    //Act
    const savedDepartment: Employee = await controller.save(createEmployeeDto);

    //Assert
    expect(service.save).toBeCalledWith(createEmployeeDto);
    expect(savedDepartment).toBe(createEmployeeDto);
  });

  it('should get All Employees by departmentId', async () => {
    //Arrange
    const departmentId = 'D1';

    //Act
    await controller.find(departmentId);

    //Assert
    expect(service.find).toBeCalledWith(departmentId);
  });
});
