import { Document } from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export type EmployeeDocument = Employee & Document;

@Schema()
export class Employee {
  id: string;

  @Prop()
  name: string;

  @Prop()
  post: string;

  @Prop()
  departmentId: string;

  @Prop()
  createdAt: Date;
}

export const EmployeeSchema = SchemaFactory.createForClass(Employee);
