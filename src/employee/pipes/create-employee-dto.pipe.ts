import {
  ArgumentMetadata,
  Injectable,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { CreateEmployeeDto } from '../dto/create-employee.dto';

@Injectable()
export class CreateEmployeeDtoPipe implements PipeTransform {
  transform(value: CreateEmployeeDto, metadata: ArgumentMetadata) {
    const schema = Joi.object({
      name: Joi.string().required(),
      post: Joi.string().required(),
      departmentId: Joi.string().required(),
    });

    const { error } = schema.validate(value);
    if (error) {
      throw new BadRequestException();
    }
    return value;
  }
}
