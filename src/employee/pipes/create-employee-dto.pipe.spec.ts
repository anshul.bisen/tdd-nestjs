import { BadRequestException } from '@nestjs/common';

import { CreateEmployeeDtoPipe } from './create-employee-dto.pipe';
import { CreateEmployeeDto } from '../dto/create-employee.dto';

describe('CreateEmployeeDtoPipe', () => {
  let pipe: CreateEmployeeDtoPipe;

  beforeEach(() => {
    pipe = new CreateEmployeeDtoPipe();
  });

  it('should be defined', () => {
    expect(new CreateEmployeeDtoPipe()).toBeDefined();
  });

  it('should convert create Employee request to Employee', () => {
    //Arrange
    const createEmployeeRequest = {
      name: 'SHIELD',
      post: 'manager',
      departmentId: 'D1',
    };

    const expectedEmployee = new CreateEmployeeDto();
    expectedEmployee.name = 'SHIELD';
    expectedEmployee.post = 'manager';
    expectedEmployee.departmentId = 'D1';

    //Act
    const transformedEmployee: CreateEmployeeDto = pipe.transform(
      createEmployeeRequest,
      null,
    );

    //Assert
    expect(transformedEmployee).toEqual(expectedEmployee);
  });

  it('should throw BadRequestException if name is empty/blank', () => {
    //Arrange
    const createEmployeeDto = {
      name: '',
      post: 'manager',
      departmentId: 'D1',
    };

    //Act
    const result = () => pipe.transform(createEmployeeDto, null);

    //Assert
    expect(result).toThrow(BadRequestException);
  });
});
