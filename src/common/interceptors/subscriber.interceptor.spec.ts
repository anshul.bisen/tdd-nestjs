import { createMock } from '@golevelup/ts-jest';
import { ExecutionContext } from '@nestjs/common';

import { SubscriberInterceptor } from './Subscriber.interceptor';

const interceptor = new SubscriberInterceptor();

const callHandler = {
  handle: jest.fn(),
};

describe('SubscriberInterceptor', () => {
  it('should be defined', () => {
    expect(interceptor).toBeDefined();
  });

  describe('intercept', () => {
    it('should return the expected result', async () => {
      // Arrange
      const mockContext = createMock<ExecutionContext>();

      // Act
      mockContext.switchToHttp().getRequest.mockReturnValueOnce({
        body: { data: 'mocked data' },
      });

      callHandler.handle.mockResolvedValueOnce('next handle');

      const actualValue = await interceptor.intercept(mockContext, callHandler);

      // Assert
      expect(actualValue).toBe('next handle');
      expect(callHandler.handle).toBeCalledTimes(1);
    });
  });
});
