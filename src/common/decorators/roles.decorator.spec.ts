import { Roles } from './roles.decorator';

describe('@Roles', () => {
  const role = 'Admin';

  class TestWithMethod {
    @Roles(role)
    public static test() {}
  }

  it('should enhance method with expected metadata', () => {
    // Arrange
    const key = 'roles';

    // Act
    const metadata = Reflect.getMetadata(key, TestWithMethod.test);

    // Assert
    expect(metadata).toEqual([role]);
  });
});
